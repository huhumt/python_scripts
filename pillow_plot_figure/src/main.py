#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PIL import Image
from re import findall
import numpy as np
import os

def createNewRGBImage(width, height, filename, RGB_color=(255,255,255)):

    """
    create new image with given size
    """

    im = Image.new("RGB", (width, height), RGB_color)
    im.save("h_"+filename)
    imv = Image.new("RGB", (width, height), RGB_color)
    imv.save("v_"+filename)

def fillRGBImage(filename, start=(0,0), size=(10,10), RGB_color=(0,0,0), v_h_flag=0):

    """
    fill rgb color to given area
    """

    im = Image.open(filename)
    np_data = np.asarray(im, dtype="int32")

    x_size = np.size(np_data, 0)
    y_size = np.size(np_data, 1)

    if v_h_flag:
        y_len = min(size[1], y_size - start[1])
        if y_len <= 0:
            return 0

        for i in range(x_size):
            for j in range(y_len):
                if start[1] + j < y_size:
                    np_data[i][start[1] + j] = list(RGB_color)
    else:
        x_len = min(size[0], x_size - start[0])
        if x_len <= 0:
            return 0

        for i in range(x_len):
            for j in range(y_size):
                if start[0] + i < x_size:
                    np_data[start[0] + i][j] = list(RGB_color)
    img = Image.fromarray(np.asarray(np.clip(np_data,0,255), dtype="uint8"), "RGB")
    img.save(filename)

def checkInputValid(size_str, mode_str, filename):

    """
    check use input valid or invalid
    """

    size_list = list(map(int, findall('\d+', size_str)))
    mode_list = list(map(int, findall('\d+', mode_str)))

    print("\n\nYour inputs:")
    print("    NAME: %s" % (filename))
    print("    SIZE: %s" % (size_str))
    print("    MODE: %s" % (mode_str))

    if len(size_list) < 2 or min(size_list) < 1:
        print("Invalid size list, must have 2 positive interger")
        exit(0)

    if len(mode_list) < 8:
        print("Invalid mode list, must have 2RGB and 2Area")
        exit(0)

    print("\n\nYour inputs:")
    print("    NAME: %s" % (filename))
    print("    SIZE: [.width=%d, .height=%d]" % (size_list[0], size_list[1]))

    mode_str = ""
    cnt = 0
    for mode_ele in mode_list:
        mode_str += str(mode_ele)
        if cnt == 3:
            mode_str += ") "
            cnt = 0
        else:
            if cnt == 0:
                mode_str += " ("
            else:
                mode_str += ","
            cnt += 1
    print("    MODE: %s" % (mode_str))

    return [filename, size_list[:2], mode_list[:(len(mode_list)//4)*4]]

def simpleInteract():

    """
    intercat with user to input some parameter
    """

    print("#########################################")
    print("   Generate figure script")
    print("       Input: WIDTH HEIGHT")
    print("       Input: 1 RGB 1 RGB")
    print("       Input: filename\n")
    print(" example:")
    print("       input: 500 400")
    print("       input: 10 (0,0,0) 10 (255,255,255)")
    print("       input: 10B10W.bmp")
    print(" this will generate 10B10W figure\n")
    print("#########################################")

    size_str = input("input figure SIZE: ")
    mode_str = input("input figure MODE: ")
    filename = input("input figure NAME: ")

    return checkInputValid(size_str, mode_str, filename)

def main():

    """
    This is main function for the project
    """

    para_list = simpleInteract()
    filename = para_list[0]
    width = para_list[1][0]
    height = para_list[1][1]
    colour_list = para_list[2]
    createNewRGBImage(width, height, filename)

    offset = 0
    while height > 0:
        for i in range(len(colour_list)//4):
            cur_area = colour_list[4*i+0]
            cur_RGB_color = (colour_list[4*i+1],colour_list[4*i+2],colour_list[4*i+3])
            fillRGBImage("h_"+filename, (offset,offset), (cur_area, cur_area), cur_RGB_color, 0)
            offset += cur_area
            height -= cur_area

    width = para_list[1][0]
    height = para_list[1][1]
    offset = 0
    while width > 0:
        for i in range(len(colour_list)//4):
            cur_area = colour_list[4*i+0]
            cur_RGB_color = (colour_list[4*i+1],colour_list[4*i+2],colour_list[4*i+3])
            fillRGBImage("v_"+filename, (offset,offset), (cur_area, cur_area), cur_RGB_color, 1)
            offset += cur_area
            width -= cur_area

if __name__ == "__main__":

    '''
    System main entry
    '''

    main()
    os.system("pause")
