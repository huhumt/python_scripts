#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tk_gui import tkGui

if __name__ == "__main__":

    '''
    System main entry
    '''

    tkGui().createMainWindow()
