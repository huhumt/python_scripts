#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def plot_figure(ch_x, ch_y, panel_type):

    """
    plot figure for target and measure
    """

    # fig, ax = plt.subplots(figsize=(6,6))
    fig, ax = plt.subplots(1)

    pad_xy = 30

    if panel_type == "Circle":
        circle_channel = min(ch_x, ch_y)
        circle_radius = pad_xy*circle_channel
        # add a circle
        outline = mpatches.Circle((circle_radius, circle_radius),
                radius=circle_radius, color='k', ls='-', fill=False)
        ax.add_patch(outline)

        start_dist = 0.2928932*circle_radius # R * (1 - sqrt(2)/2)
        stop_dist  = 2*circle_radius - start_dist
        delta_dist = (stop_dist - start_dist) / circle_channel

        for x in range(0, circle_channel+1):
            plot1, = ax.plot([start_dist+x*delta_dist, start_dist+x*delta_dist], [start_dist, stop_dist], 'k--', linewidth=0.8)
        for y in range(0, circle_channel+1):
            plot1, = ax.plot([start_dist, stop_dist], [start_dist+y*delta_dist, start_dist+y*delta_dist], 'k--', linewidth=0.8)
    else:
        # add a rectangle
        outline = mpatches.Rectangle((0, 0),
                width=pad_xy*ch_x, height=pad_xy*ch_y,
                color='k', ls='-', fill=False)
        ax.add_patch(outline)

        for x in range(1, ch_x):
            plot1, = ax.plot([pad_xy*x, pad_xy*x], [0, pad_xy*ch_y], 'k-', linewidth=0.8)
        for y in range(1, ch_y):
            plot1, = ax.plot([0, pad_xy*ch_x], [pad_xy*y, pad_xy*y], 'k-', linewidth=0.8)

    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    ax.axis('tight')
    ax.axis('off')
    plt.savefig("ch_mapping.png", format="png", transparent=True)
    # plt.show()
    plt.clf()
    plt.cla()
    plt.close()

if __name__ == "__main__":

    """
    for test purpose
    """

    x_list = [[1, 100], [1, 100]]
    y_list = [[1, 100], [100, 1]]

    plot_figure(x_list, y_list, panel_type="rect")
