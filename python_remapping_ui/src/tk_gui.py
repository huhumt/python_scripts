#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import ttk
from PIL import Image, ImageTk
from plot_figure import plot_figure
from re import findall

def generate_remapping_info(config_list, ori_list, attr_list):

    """
    generate info
    """

    ch_x = config_list[2][0]
    ch_y = config_list[3][0]
    edge_ch_x = config_list[4][0]
    edge_ch_y = config_list[5][0]
    panel_type = config_list[-1][0]

    output_list = [[] for i in range(ch_y)]
    div_to_dict = {"div_0" : 0, "div_1to2" : 2, "div_1to3" : 3, "div_1to4" : 4}

    try:
        if panel_type == "Circle":
            center_ch = min(ch_x, ch_y)

            if (ch_x * ch_y) != (center_ch * center_ch) + (edge_ch_x + edge_ch_y) * 2:
                raise ValueError("channels do not equal")

            for i in range(center_ch * center_ch):
                tx = i // center_ch
                rx = i %  center_ch
                # ch_num, div_dir, div_pos, div_to, tx, rx
                output_list[tx].append([ori_list[i], 0, 0, 0, tx+1, rx+1])

            for i in range(center_ch * center_ch, ch_y * ch_x):
                #define CH_DIV_DIR_NONE         (0x0)
                #define CH_DIV_DIR_TX           (0x1)
                #define CH_DIV_DIR_RX           (0x2)
                #define CH_DIV_DIR_TXRX         (0x3)

                #define CH_DIV_POS_NONE         (0x0)
                #define CH_DIV_POS_TOP          (0x1)
                #define CH_DIV_POS_LEFT         (0x2)
                #define CH_DIV_POS_BOTTOM       (0x3)
                #define CH_DIV_POS_RIGHT        (0x4)
                div_dir = 0
                div_pos = 0
                div_to  = 0
                tx      = 0
                rx      = 0
                if "Top edge" in attr_list[i]:
                    div_dir = 1
                    div_pos = 1
                    div_to  = div_to_dict[config_list[6][0]]
                    tx      = 0
                    if attr_list[i] == "Top edge center":
                        if edge_ch_x > 1:
                            div_to  = 0
                        rx = (center_ch + 1) // 2 + 1 - div_to // 2
                    elif attr_list[i] == "Top edge left":
                        rx = 0
                    elif attr_list[i] == "Top edge right":
                        rx = 0
                elif "Left edge" in attr_list[i]:
                    div_dir = 2
                    div_pos = 2
                    div_to  = div_to_dict[config_list[7][0]]
                    rx      = 0
                elif "Bottom edge" in attr_list[i]:
                    div_dir = 1
                    div_pos = 3
                    div_to  = div_to_dict[config_list[6][0]]
                    tx      = ch_y - 1
                elif "Right edge" in attr_list[i]:
                    div_dir = 2
                    div_pos = 4
                    div_to  = div_to_dict[config_list[7][0]]
                    rx      = ch_x - 1

                if div_pos > 0:
                    # ch_num, div_dir, div_pos, div_to, tx, rx
                    output_list[div_pos-1].append([ori_list[i], div_dir, div_pos, div_to, tx, rx])
        else:
            for y in range(ch_y):
                for x in range(ch_x):
                    output_list[y].append([ori_list[y*ch_x+x], 0, 0, 0, y, x])

        mapping_str = "\n#define ORI_CH_MAPPING    { \\\n"

        for y in range(ch_y):
            line_str = "    "
            for x in range(ch_x):
                line_str += ("CH" + output_list[y][x][0] + ", ")
            mapping_str += (line_str + "\\\n")
        mapping_str += "}"

        mapping_str += "\n\n#define DIV_CH_MAPPING    { \\\n"
        for y in range(ch_y):
            line_str = "    "
            for x in range(ch_x):
                # ch_num, div_dir, div_pos, div_to, tx, rx
                div_dir = output_list[y][x][1]
                div_pos = output_list[y][x][2]
                div_to  = output_list[y][x][3]
                tx      = output_list[y][x][4]
                rx      = output_list[y][x][5]
                ch_info = '{:04x}'.format((div_dir << 14) + (div_pos << 11) + (div_to << 8) + (tx << 4) + rx)
                line_str += ("0x" + ch_info + ", ")
            mapping_str += (line_str + "\\\n")
        mapping_str += "}"

    except:
        mapping_str = "\n----------------------------------------------\n"
        mapping_str += "\n       Invalid channel configuration          \n"
        mapping_str += "\n----------------------------------------------\n"
        pass

    return mapping_str

class tkGui:
    '''
    create tk gui
    '''

    def __init__(self, name="Main windows"):
        self.name = name
        self.plotframe = None
        self.outstring = "Hello, world"
        self.ori_ch_mapping = []
        self.pre_config_list = []
        self.entry_list = []
        self.attr_list = []

    def isDigitStr(self, input_str):
        return input_str.isdigit()

    def createMainWindow(self):
        self.root = Tk()
        self.root.title("FT2389 circle panel CH_Flag generator ---Poweblue by Focaltech")
        self.root.geometry("680x620")

        mainframe = ttk.Frame(self.root,
                cursor="circle", relief="flat", borderwidth=5,
                padding=(70, 3, 0, 0))
        # mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        mainframe.grid(column=0, row=0, sticky=(N, W))

        configframe_Style = ttk.Style()
        configframe_Style.configure("config.TFrame", background="grey")

        configframe = ttk.Frame(mainframe, cursor="cross",
                height=400, width=400, borderwidth=2, style="config.TFrame",
                padding=(0, 10, 10, 10))
        configframe.grid(column=0, row=0, columnspan=5, rowspan=5,
                padx=5, pady=20, sticky=(N, W, E, S))

        config_Lable = ttk.Label(configframe,
                font=('Times', '14', 'italic', 'bold', 'underline'),
                text="Config:", foreground="blue", background="grey",
                padding=(0, 0, 0, 14))
        config_Lable.grid(column=0, row=0)

        pix_X = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="Resolution_X", foreground="blue", background="grey",
                padding=(25, 0, 1, 0))
        pix_X.grid(column=0, row=1)

        pix_X_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        pix_X_entry.grid(column=1, row=1)
        pix_X_entry.insert(0, "600")

        pix_Y = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="Resolution_Y", foreground="blue", background="grey",
                padding=(15, 0, 1, 0))
        pix_Y.grid(column=2, row=1)

        pix_Y_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        pix_Y_entry.grid(column=3, row=1)
        pix_Y_entry.insert(0, "600")

        ch_X = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="ChannelX_Num\nRx_Num", foreground="blue", background="grey",
                justify="center", padding=(25, 20, 1, 20))
        ch_X.grid(column=0, row=2)

        ch_X_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        ch_X_entry.grid(column=1, row=2)
        ch_X_entry.insert(0, "6")

        ch_Y = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="ChannelY_Num\nTx_Num", foreground="blue", background="grey",
                justify="center", padding=(15, 20, 1, 20))
        ch_Y.grid(column=2, row=2)

        ch_Y_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        ch_Y_entry.grid(column=3, row=2)
        ch_Y_entry.insert(0, "4")

        edge_ch_X = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="Edge_ChX_Num", foreground="blue", background="grey",
                padding=(25, 0, 1, 0))
        edge_ch_X.grid(column=0, row=3)

        edge_ch_X_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        edge_ch_X_entry.grid(column=1, row=3)
        edge_ch_X_entry.insert(0, "2")

        edge_x_div_to_combo = ttk.Combobox(configframe,
                height=10, width=10, justify="center",
                values=["div_0", "div_1to2", "div_1to3", "div_1to4"])
        edge_x_div_to_combo.grid(column=1, row=4)
        edge_x_div_to_combo.insert(0, "div_1to2")

        edge_ch_Y = ttk.Label(configframe,
                font=('Times', '8', 'italic'),
                text="Edge_ChY_Num", foreground="blue", background="grey",
                padding=(15, 0, 1, 0))
        edge_ch_Y.grid(column=2, row=3)

        edge_y_div_to_combo = ttk.Combobox(configframe,
                height=10, width=10, justify="center",
                values=["div_0", "div_1to2", "div_1to3", "div_1to4"])
        edge_y_div_to_combo.grid(column=3, row=4)
        edge_y_div_to_combo.insert(0, "div_1to2")

        edge_ch_Y_entry = ttk.Entry(configframe, width=8,
                font=('Times', '10', 'bold'), justify="center",
                validate='all',
                validatecommand=(configframe.register(self.isDigitStr), '%S'))
        edge_ch_Y_entry.grid(column=3, row=3)
        edge_ch_Y_entry.insert(0, "2")

        generateframe = ttk.Frame(mainframe, cursor="cross",
                height=80, width=80, borderwidth=2,
                padding=(0, 45, 10, 10))
        generateframe.grid(column=10, row=0, columnspan=5, rowspan=5,
                padx=5, pady=20, sticky=(N, W, E, S))

        shape = ttk.Label(generateframe,
                font=('Times', '8', 'italic'),
                text="Shape", foreground="blue",
                padding=(5, 15, 1, 15))
        shape.grid(column=1, row=0)

        shape_combo = ttk.Combobox(generateframe,
                height=10, width=10, justify="center",
                values=["Circle", "Rectangle"])
        shape_combo.grid(column=2, row=0)
        shape_combo.insert(0, "Circle")

        generate_button = ttk.Button(generateframe, text="Generate",
                cursor="arrow", width=15, padding=(0, 12, 0, 12),
                command=lambda : self.createPlotFrame(
                    [
                        [pix_X_entry], [pix_Y_entry],
                        [ch_X_entry], [ch_Y_entry],
                        [edge_ch_X_entry], [edge_ch_Y_entry],
                        [edge_x_div_to_combo], [edge_y_div_to_combo],
                        [shape_combo]
                        ]
                    )
                )
        generate_button.grid(column=2, row=1)

        self.copy_button = ttk.Button(generateframe, text="Copy",
                cursor="arrow", width=15, padding=(0, 12, 0, 12),
                command=self.copy_text_to_clipboard)
        self.copy_button.grid(column=2, row=2)

        # self.createPlotFrame([])
        # self.root.columnconfigure(0, weight=1)
        # self.root.rowconfigure(0, weight=1)
        self.root.mainloop()

    def createPlotFrame(self, cur_config_list):
        config_list = [[600], [600], [5], [4], [0], [0], ["div_1to2"], ["div_1to2"], ["Circle"]]

        for i in range(len(config_list)):
            cur_par = cur_config_list[i][0].get()
            if len(cur_par) > 0:
                if i > 5: # circle or rectangle
                    config_list[i][0] = cur_par
                else:
                    if cur_par.isdigit():
                        cur_int = int(cur_par)
                        if cur_int > 0:
                            config_list[i][0] = cur_int

        for i in range(len(config_list)):
            cur_config_list[i][0].delete(0, END)
            cur_config_list[i][0].insert(0, config_list[i][0])

        plot_figure(config_list[2][0], config_list[3][0], config_list[-1][0])

        if self.plotframe:
            for i in range(len(self.entry_list)):
                ch_info = findall('\d+', self.entry_list[i].get())
                if len(ch_info) > 0:
                    self.ori_ch_mapping[i] = '{:02d}'.format(int(ch_info[-1]))
            self.plotframe.destroy()
            self.entry_list = []

        for i in range(len(self.pre_config_list)):
            if config_list[i][0] != self.pre_config_list[i][0]:
                self.ori_ch_mapping = []
                break
        self.pre_config_list = config_list.copy()
        self.plotframe = ttk.Frame(self.root, height=300, width=600)
        self.plotframe.grid(column=0, row=1, sticky=(N, W))

        img = Image.open("ch_mapping.png").resize((350,350))
        img = ImageTk.PhotoImage(img)

        img_canvas = Canvas(self.plotframe, cursor="arrow",
                width=350, height=350)
        img_canvas.create_image(0, 0, image=img, anchor="nw")
        self.canvas_plot_text(img_canvas, config_list)
        img_canvas.grid(column=0, row=0)

        remapping_info = generate_remapping_info(config_list, self.ori_ch_mapping, self.attr_list)
        out_info_font_size = str(min(max(14 - max(config_list[2][0], config_list[3][0]), 6), 9))
        out_info = ttk.Label(self.plotframe, justify="left",
                padding=(0, 0, 0, 0), cursor="xterm",
                font=('Times', out_info_font_size), foreground="green",
                relief="solid", borderwidth=2, takefocus=True,
                text=remapping_info)
        out_info.grid(column=1, row=0)
        self.outstring = out_info.cget("text")

        self.root.mainloop()

    def copy_text_to_clipboard(self):
        self.popup_msg(
                "Channel information has been successfully copied to clipborad",
                1800)
        self.root.clipboard_clear()
        self.root.clipboard_append(self.outstring)
        self.root.mainloop()

    def popup_msg(self, msg, timout=1800):
        msg_label = ttk.Label(self.root, justify="left",
                padding=(0, 0, 0, 0), font=('Times', 15),
                background = "red", foreground="green", text=msg)
        msg_label.grid(column=0, row=1)

        msg_label.after(1800, msg_label.destroy)

    def canvas_plot_text(self, canvas, config_list):
        x_width  = int(canvas.cget("width"))
        y_height = int(canvas.cget("height"))
        ch_x = config_list[2][0]
        ch_y = config_list[3][0]
        edge_ch_x = config_list[4][0]
        edge_ch_y = config_list[5][0]
        panel_type = config_list[-1][0]

        font_size = str(min(max(17-max(ch_x, ch_y), 4), 9))
        ch_mapping_len = len(self.ori_ch_mapping)
        self.entry_list = []
        self.attr_list = []

        if panel_type == "Circle":
            circle_channel = min(ch_x, ch_y)
            circle_radius = min(x_width, y_height)
            delta_x_width = (circle_radius * 9 / 10) / (circle_channel * 1.414)
            delta_y_height = delta_x_width
            ch_num = 1

            for i in range(circle_channel):
                for j in range(circle_channel):
                    x = circle_radius / 2 + (j - circle_channel // 2) * delta_x_width
                    y = circle_radius / 2 + (i - circle_channel // 2) * delta_y_height

                    if (circle_channel & 0x01) == 0:
                        x += (delta_x_width / 2)
                        y += (delta_y_height / 2)

                    # canvas.create_text(x+2, y+2, font=('Times', font_size),
                    #         anchor="center", justify="center", text="haha")
                    self.create_entry_in_canvas(canvas, x, y, ch_num,
                            delta_x_width*7/8, delta_y_height/2, ch_mapping_len)
                    self.attr_list.append("Center")
                    ch_num += 1

            neg_offset = circle_radius / 2 - (circle_channel / 2 + 0.46) * delta_x_width
            pos_offset = circle_radius / 2 + (circle_channel / 2 + 0.46) * delta_x_width
            w_h_size = min(max(delta_x_width * 7 / 8, 15), 30)

            if (edge_ch_x & 0x01) == 1:
                self.create_entry_in_canvas(canvas, circle_radius / 2, neg_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Top edge center")
                ch_num += 1
                self.create_entry_in_canvas(canvas, circle_radius / 2, pos_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Bottom edge center")
                ch_num += 1

            for i in range(edge_ch_x // 2):
                left_x  = circle_radius / 2 - i * delta_x_width
                right_x = circle_radius / 2 + i * delta_x_width
                if (edge_ch_x & 0x01) == 0:
                    left_x  -= (delta_x_width / 2)
                    right_x += (delta_x_width / 2)
                else:
                    left_x  -= (delta_x_width)
                    right_x += (delta_x_width)
                self.create_entry_in_canvas(canvas, left_x, neg_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Top edge left")
                ch_num += 1
                self.create_entry_in_canvas(canvas, left_x, pos_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Bottom edge left")
                ch_num += 1
                self.create_entry_in_canvas(canvas, right_x, neg_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Top edge right")
                ch_num += 1
                self.create_entry_in_canvas(canvas, right_x, pos_offset, ch_num,
                        delta_x_width*7/8, w_h_size, ch_mapping_len)
                self.attr_list.append("Bottom edge right")
                ch_num += 1

            if (edge_ch_y & 0x01) == 1:
                self.create_entry_in_canvas(canvas, neg_offset, circle_radius / 2, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Left edge center")
                ch_num += 1
                self.create_entry_in_canvas(canvas, pos_offset, circle_radius / 2, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Right edge center")
                ch_num += 1

            for i in range(edge_ch_y // 2):
                upper_y  = circle_radius / 2 - i * delta_y_height
                bottom_y = circle_radius / 2 + i * delta_y_height
                if (edge_ch_y & 0x01) == 0:
                    upper_y  -= (delta_y_height / 2)
                    bottom_y += (delta_y_height / 2)
                else:
                    upper_y  -= (delta_y_height)
                    bottom_y += (delta_y_height)
                self.create_entry_in_canvas(canvas, neg_offset, upper_y, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Left edge upper")
                ch_num += 1
                self.create_entry_in_canvas(canvas, pos_offset, upper_y, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Right edge upper")
                ch_num += 1
                self.create_entry_in_canvas(canvas, neg_offset, bottom_y, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Left edge bottom")
                ch_num += 1
                self.create_entry_in_canvas(canvas, pos_offset, bottom_y, ch_num,
                        w_h_size, delta_y_height*7/8, ch_mapping_len)
                self.attr_list.append("Right edge bottom")
                ch_num += 1

            empty_ch_num = ch_x * ch_y + 1 - ch_num
            for i in range(empty_ch_num):
                self.create_entry_in_canvas(canvas,
                        circle_radius/2 - (empty_ch_num//2 - i)*delta_x_width/2,
                        delta_y_height / 6, ch_num,
                        delta_x_width*3/5, delta_y_height/2, ch_mapping_len)
                self.attr_list.append("Empty channel")
                ch_num += 1
        else:
            delta_x_width = (x_width * 9 / 10) / ch_x
            delta_y_height = (y_height * 9 / 10) / ch_y

            for i in range(ch_y):
                for j in range(ch_x):
                    x = x_width / 2 + (j - ch_x // 2) * delta_x_width
                    y = y_height / 2 + (i - ch_y // 2) * delta_y_height
                    if (ch_x & 0x01) == 0:
                        x += (delta_x_width / 2)
                    if (ch_y & 0x01) == 0:
                        y += (delta_y_height / 2)
                    # canvas.create_text(x+2, y+2, font=('Times', font_size),
                    #         anchor="center", justify="center", text="haha")
                    self.create_entry_in_canvas(canvas, x, y, i*ch_x+j+1,
                            delta_x_width*7/8, delta_y_height/2, ch_mapping_len)
                    self.attr_list.append("Center")

    def create_entry_in_canvas(self, canvas, x, y, pos, width, height, flag):
        if flag:
            ch_info = '{:02d}'.format(int(findall('\d+', self.ori_ch_mapping[pos-1])[-1]))
            self.ori_ch_mapping[pos-1] = ch_info
        else:
            ch_info = '{:02d}'.format(pos)
            self.ori_ch_mapping.append(ch_info)
        entry = ttk.Entry(self.plotframe, width=4,
                font=('Times', '7'), justify="center",
                )
        entry.delete(0, END)
        entry.insert(0, "S" + ch_info)
        entry_style = ttk.Style()
        entry_style.configure("duplicate.TEntry", foreground="red")

        if self.ori_ch_mapping.count(ch_info) > 1:
            entry.configure(style="duplicate.TEntry", font=('Times', '7', 'bold', 'italic'))
        self.entry_list.append(entry)
        canvas.create_window(x, y,
                width=width, height=height,
                anchor="center", window=entry)

if __name__ == "__main__":

    '''
    System main entry
    '''

    tkGui().createMainWindow()
